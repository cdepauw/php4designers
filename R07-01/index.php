<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
    </head>
    <body>
        <h2>Storten en afhalen</h2>
        <?php

        function transactie($saldo, $som, $type = "storting") {
            $nieuwsaldo = $saldo;
            if ($type == "storting") {
                $nieuwsaldo += $som;
            } else {
                $nieuwsaldo -= $som;
            }
            return $nieuwsaldo;
        }

        $som = 1000;
        $beginsaldo = bankrek();
        $type = "storting";
        $saldo = transactie($beginsaldo, $som, $type);
        echo "Door de $type van $som bedraagt het nieuwesaldo $saldo.<br />";
        echo "Het saldo voor de storting was $beginsaldo.";
        ?>   

        <?php
            function bankrek () {
                $beginsaldo = mt_rand(-10000, 10000);
                return $beginsaldo;
            }
        ?>
    </body>
</html>
